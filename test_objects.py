from analysis_tools import *
p1 = Process("example1", "EXAMPLE1")
p2 = Process("example2", "EXAMPLE2")

O1 = ObjectCollection([p1])
O2 = ObjectCollection([p2])
Otot = ObjectCollection([p1, p2])

Osum = O1 + O2
if not str(Osum) == str(Otot):
    raise Exception("Error while running __add__ method")

O1 += O2
if not str(O1) == str(Otot):
    raise Exception("Error while running __iadd__ method")

assert str(O1[0]) == str(p1)

assert len(O1) == 2


d1 = Dataset("test_dataset_1",
    folder = "data/test_ntuples/",
    process=p1,
)

d2 = Dataset("test_dataset_2",
    folder = "data/test_ntuples/",
    process=p1,
    skipFiles=["data/test_ntuples/file_0.root"],
)

d3 = Dataset("test_dataset_3",
    folder = "data/test_ntuples/",
    process=p1,
    skipFiles=["data/test_ntuples/file_10.root"],
)

d4 = Dataset("test_dataset_4",
    folder = "data/test_ntuples/",
    process=p1,
    skipFiles=["data/test_ntuples/file_10.root"],
    skipped_files_must_be_in_dataset=False
)

assert len(d1.get_files()) == 2
assert len(d2.get_files()) == 1
while True:
    try:
        d3.get_files()
    except Exception:
        break
assert len(d4.get_files()) == 2
        
