import os
from contextlib import contextmanager

def randomize(input_string):
    import random, string
    random_str = "".join(random.choice(string.ascii_letters) for _ in range(10))
    return "{}_{}".format(input_string, random_str)


def import_root(batch=True):
    import ROOT
    ROOT.PyConfig.IgnoreCommandLineOptions = True
    ROOT.gROOT.SetBatch(batch)

    return ROOT


def create_file_dir(file_path):
    file_path = os.path.expandvars(os.path.expanduser(file_path))
    file_dir = os.path.dirname(file_path)
    if not os.path.exists(file_dir):
        try:
            os.makedirs(file_dir)
        except OSError as err:
            print("%s, skipping directory creation" % err)
    return file_path


def join_root_selection(selection1, selection2=None, op="and"):
    if op == "and":
        op = " && "
    elif op == "or":
        op = " || "
    elif op == "*":
        op = " * "
    else:
        raise ValueError("Operator {} does not exist".format(op))
    args_to_join = []
    for selection in [selection1, selection2]:
        if selection and isinstance(selection, list):
            args_to_join.extend(selection)
        elif selection and not isinstance(selection, list):
            args_to_join.append(selection)
    
    return op.join(["({})".format(arg) for arg in args_to_join])


class DotDict(dict):
    __getattr__ = dict.__getitem__
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__


# histo tools
def getContentHisto2D(TH2, x, y, unc=0):
    nbinsx = TH2.GetNbinsX()
    nbinsy = TH2.GetNbinsY()

    ibinx = TH2.GetXaxis().FindBin(x)
    ibiny = TH2.GetYaxis().FindBin(y)

    if (ibinx == 0):
        ibinx = 1
    elif ibinx > nbinsx:
        ibinx = nbinsx

    if (ibiny == 0):
        ibiny = 1
    elif ibiny > nbinsy:
        ibiny = nbinsy

    if not unc:
        return TH2.GetBinContent(ibinx, ibiny)
    else:
        return TH2.GetBinError(ibinx, ibiny)

def getContentHisto3D(TH3, x, y, z, unc=0):
    nbinsx = TH3.GetNbinsX()
    nbinsy = TH3.GetNbinsY()
    nbinsz = TH3.GetNbinsZ()

    ibinx = TH3.GetXaxis().FindBin(x)
    ibiny = TH3.GetYaxis().FindBin(y)
    ibinz = TH3.GetZaxis().FindBin(z)

    if (ibinx == 0):
        ibinx = 1
    elif ibinx > nbinsx:
        ibinx = nbinsx

    if (ibiny == 0):
        ibiny = 1
    elif ibiny > nbinsy:
        ibiny = nbinsy

    if (ibinz == 0):
        ibinz = 1
    elif ibinz > nbinsz:
        ibinz = nbinsz

    if not unc:
        return TH3.GetBinContent(ibinx, ibiny, ibinz)
    else:
        return TH3.GetBinError(ibinx, ibiny, ibinz)


def openTFile(filename, mode = 'READ'):
  tfile = ROOT.TFile(filename, mode)
  try: # We want to close the tfile even if an exception happened
    yield tfile
  finally:
    tfile.Close()


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
