class BaseObject(object):
    def __init__(self, name, *args, **kwargs):
        self.name = name
        self.aux = kwargs

    def get_aux(self, field, default_value=None):
        if field in self.aux:
            return self.aux[field]
        else:
            return default_value

class TaggedObject(BaseObject):
    def __init__(self, name, *args, **kwargs):
        self.tags = kwargs.pop("tags", [])
        super(TaggedObject, self).__init__(name, *args, **kwargs)

    def has_tag(self, tag):
        if isinstance(tag, list):
            # requiring an OR of the input tags
            return any([self.has_tag(t) for t in tag])
        if tag in self.tags:
            return True
        return False