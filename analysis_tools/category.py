from analysis_tools.base_object import BaseObject


class Category(BaseObject):
    def __init__(self, name, label, *args, **kwargs):
        self.label = label
        self.subcategories = kwargs.pop("subcategories", None)
        self.base_category = kwargs.pop("base_category", None)
        self.selection = kwargs.pop("selection", None)
        super(Category, self).__init__(name, *args, **kwargs)

    def __repr__(self):
        return "Category(name=%s, label=%s)" % (self.name, self.label)
