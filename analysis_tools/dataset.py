import os
import json
from tqdm import tqdm
from subprocess import call
from copy import deepcopy
import re
from analysis_tools.utils import randomize, create_file_dir, import_root, bcolors
from analysis_tools.base_object import TaggedObject
from analysis_tools.process import Process


def open_and_check_file(filename):
    ROOT = import_root()
    tf = ROOT.TFile.Open(filename)
    keys = tf.GetListOfKeys()


class Dataset(TaggedObject):
    def __init__(self, name, folder=None, dataset=None, process=None, *args, **kwargs):
        if isinstance(name, Dataset):
            d = dir(name)
            for elem in d:
                if elem.startswith("__") or elem in kwargs or elem == "aux":
                    continue
                atrib = getattr(name, elem)
                if str(type(atrib)) != "<type 'instancemethod'>":
                    kwargs[elem] = atrib
            if "dataset_name" in kwargs:
                kwargs["name"] = kwargs.pop("dataset_name")

            Dataset.__init__(self, **kwargs)
        else:
            assert isinstance(process, Process)
            assert folder or dataset
            self.process = process
            self.folder = folder
            self.dataset = dataset
            self.prefix = kwargs.pop("prefix", None)
            self.locate = kwargs.pop("locate", None)
            if not self.locate and self.prefix:
                self.locate = self.prefix
            self.runPeriod = kwargs.pop("runPeriod", "")
            self.runEra = kwargs.pop("runEra", "dum")
            self.skip_files = kwargs.pop("skipFiles", None)
            self.xs = kwargs.pop("xs", 1.)
            self.treename = kwargs.pop("treename", "Events")
            self.file_pattern = kwargs.pop("file_pattern", None)
            self.friend_datasets = kwargs.pop("friend_datasets", None)
            self.check_empty = kwargs.pop("check_empty", True)
            self.skipped_files_must_be_in_dataset = kwargs.pop("skipped_files_must_be_in_dataset",
                True)
            super(Dataset, self).__init__(name, *args, **kwargs)

    def __repr__(self):
        return "Dataset(name=%s, process=%s)" % (self.name, self.process)

    def locate_files(self, folder=None):
        folder = self.folder if not folder else folder
        print(folder)
        files = []

        if type(folder) == list:
            for fold in folder:
                files += self.locate_files(fold)
            return files

        lines = None
        cmd = None
        if not self.dataset and not self.locate:
            if not os.path.isdir(folder):
                return []
            lines = [os.path.join(folder, elem) for elem in os.listdir(folder)]
        elif self.dataset:
            filename = randomize("./tmp") + ".txt"
            cmd = "dasgoclient --query='file dataset=%s {}' --limit=0 > %s" % (
                self.dataset, filename)
            for dbs in ["", "instance=prod/phys01", "instance=prod/phys02", "instance=prod/phys03"]:
                lines = []
                rc = call(cmd.format(dbs), shell = True)
                if rc == 0:
                    with open(filename) as f:
                        lines = [file.strip() for file in f.readlines()]
                os.remove(filename)
                if len(lines) != 0:
                    break
        elif self.locate:
            # First check whether it is an actual folder
            cmd = "xrdfs {} stat {} -q IsDir > /dev/null".format(self.locate, folder)
            rc = call(cmd, shell = True)
            if rc != 0:  # error -> not a folder
                return []

            filename = randomize("./tmp") + ".txt"
            cmd = "xrdfs {} ls {} > {}".format(self.locate, folder, filename)
            rc = call(cmd, shell = True)
            if rc == 0:
                with open(filename) as f:
                    lines = [file.strip() for file in f.readlines()]
            os.remove(filename)

        for line in lines:
            # line = "{}/{}".format(folder, line) if folder else line
            if ".root" not in line:
                files += self.locate_files(folder=line)
            else:
                if self.file_pattern:
                    filename = line.split("/")[-1]
                    if (re.fullmatch(self.file_pattern, filename) is None):
                        continue
                files += [line]
        return files

    def get_files(self, path_to_look=None, index=None, add_prefix=True, check_empty=False):
        # avoid checking for empty files if the user explicitly said it so
        check_empty = check_empty and self.check_empty
        # we cannot add a prefix if it's not a grid dataset or it is specified
        add_prefix = add_prefix and (self.dataset or self.prefix)
        found = False
        if path_to_look:
            if os.path.isdir(path_to_look):
                # First check that the DAS dataset/folder used in this dataset hasn't changed
                if f"{self.name}_dataset.txt" in os.listdir(path_to_look):
                    with open(f"{path_to_look}/{self.name}_dataset.txt") as f:
                        datasets = f.readlines()
                    if self.dataset:
                        if len(datasets) != 1 or datasets[0].strip() != self.dataset:
                            raise ValueError(
                                f"The dataset defined inside dataset {self.name} has changed. Please remove " +
                                f"{os.path.join(path_to_look, self.name)}.txt and "
                                f"{os.path.join(path_to_look, self.name)}_dataset.txt "
                                "to continue running."
                            )
                    else: # self.folder
                        if isinstance(self.folder, list):
                            matches = [elem.strip() in self.folder for elem in datasets]
                            if not all(matches) or len(datasets) != len(self.folder):
                                raise ValueError(
                                    f"The folders defined inside dataset {self.name} have changed. "+
                                    f"Please remove {os.path.join(path_to_look, self.name)}.txt to "
                                    "continue running."
                                )
                        else:
                            if len(datasets) != 1 or datasets[0].strip() != self.folder:
                                raise ValueError(
                                    f"The folder defined inside dataset {self.name} has changed. " +
                                    f"Please remove {os.path.join(path_to_look, self.name)}.txt to "
                                    "continue running."
                                )

                if "%s.txt" % self.name in os.listdir(path_to_look):
                    with open(f"{path_to_look}/{self.name}.txt") as f:
                        files = f.readlines()
                    found = True
        if not found:
            files = deepcopy(self.locate_files())
            if check_empty:
                ROOT = import_root()

                # loop over all files
                aux_files = deepcopy(files)
                for ifil, f in tqdm(enumerate(aux_files), total=len(aux_files),
                        desc="Looking for empty files in dataset " + self.name):

                    # Add prefix if needed
                    if self.prefix and (self.dataset or self.prefix) and self.prefix not in files[0]:
                        file_to_open = "root://" + self.prefix + f
                    # if not, but still need to be added, look for the best one
                    # between eu, global or na
                    elif (self.dataset or self.prefix) and "root://" not in files[0]:
                        file_to_open = self.add_prefix(f) + f
                    else:
                        file_to_open = f

                    try:
                        tf = ROOT.TFile.Open(file_to_open)
                        tree = tf.Get(self.treename)
                        if tree.GetEntries() <= 0:
                            files.remove(f)
                        tf.Close()
                    except:  # no tree available, remove as well
                        try:
                            files.remove(f)
                        except ValueError:
                            raise ValueError(f"{f} not in files")

        # remove EOF if needed
        files = [file.strip() for file in files]

        # avoid having no-file-datasets
        if not files:
            raise Exception("No files were found for dataset %s" % self.name)

        # save filenames in the path introduced if specified
        if path_to_look and not found:
            with open(create_file_dir(f"{path_to_look}/{self.name}.txt"), "w+") as f:
                files_to_write = "\n".join(files)
                f.write(files_to_write)
        if path_to_look:
            if not f"{self.name}_dataset.txt" in os.listdir(path_to_look):
                with open(create_file_dir(f"{path_to_look}/{self.name}_dataset.txt"), "w+") as f:
                    if self.dataset:
                        f.write(self.dataset)
                    elif isinstance(self.folder, list):
                        folders_to_write = "\n".join(self.folder)
                        f.write(folders_to_write)
                    else:
                        f.write(self.folder)

        # skip files if specified in the declaration
        files_to_skip = deepcopy(self.skip_files)
        if files_to_skip:
            if not self.skipped_files_must_be_in_dataset:
                print(f"{bcolors.WARNING}WARNING: Dataset {self.name} "
                    "has skipped_files_must_be_in_dataset=False. This option may lead to not "
                    f"removing the desired files, use it with care.{bcolors.ENDC}")
            for file in self.skip_files:
                if file in files:
                    files.remove(file)
                    files_to_skip.remove(file)
            if len(files_to_skip) > 0 and self.skipped_files_must_be_in_dataset:
                raise Exception("Files", files_to_skip, "do not exist in dataset")

        files.sort()

        # if index is specified, select only that file
        if index != None:
            files = [files[index]]

        # if the prefix is specified, add it if specified
        if self.prefix and add_prefix and self.prefix not in files[0]:
            files = [("root://" + self.prefix + f) for f in files]
        # if not, but still need to be added, look for the best one between eu, global or na
        elif add_prefix and "root://" not in files[0]:
            files = [(self.add_prefix(f) + f) for f in files]
        if index != None:
            return files[0]
        return files

    def add_prefix(self, filename):
        import time

        prefixes = ["root://cms-xrd-global.cern.ch//", "root://xrootd-cms.infn.it//",
            "root://cmsxrootd.fnal.gov//"]

        times = []
        for prefix in prefixes:
            t0 = time.time()
            try:
                open_and_check_file(prefix + filename)
                times.append(time.time() - t0)
            except:
                times.append(999)
        return prefixes[times.index(min(times))]

    def get_file_groups(self, threshold=None, path_to_look=None):
        ROOT = import_root()
        groups = []
        if path_to_look:
            if os.path.isdir("%s/grouped/" % path_to_look):
                if "%s.txt" % self.name in os.listdir("%s/grouped/" % path_to_look):
                    with open("%s/grouped/%s.txt" % (path_to_look, self.name)) as f:
                        grouped_files = [line.strip() for line in f.readlines()]
                    if len(grouped_files[-1]) == 0:
                        grouped_files = grouped_files[:-1]
                    groups = [[int(elem) for elem in group.split(" ")]
                        for group in grouped_files]

        if not groups:
            if self.dataset:
                cmd = 'dasgoclient --query=="file dataset={}" --json > {}'
                tmpname = randomize("tmp")
                rc = call(cmd.format(self.dataset, tmpname), shell = True)
                if rc == 0:
                    with open(tmpname) as f:
                        d = json.load(f)
                        os.remove(tmpname)
                file_list = []
                try:
                    for ielem, elem in enumerate(d):
                        file_list.append((ielem, int(elem["file"][0]["nevents"])))
                except UnboundLocalError:
                    print("It seems the dasgoclient command didn't work. "
                        "Probably you should have obtained your proxy")
                    sys.exit()

            elif self.folder:
                file_list = []
                files = os.listdir(self.folder)
                for ifile, f in enumerate(files):
                    df = ROOT.RDataFrame(self.treename, os.path.join(self.folder, f))
                    file_list.append((ifile, df.Count().GetValue()))
            file_list.sort(key=lambda x: x[1], reverse=True)
            max_j = len(file_list) - 1
            for i in range(len(file_list)):
                if i > max_j:
                    continue
                this_group = [file_list[i][0]]
                nevents = file_list[i][1]
                for j in range(max_j, i, -1):
                    if nevents + file_list[j][1] > threshold:
                        max_j = j
                        break
                    else:
                        this_group.append(file_list[j][0])
                        nevents += file_list[j][1]
                        if j == i + 1:  # Grouping one file and the following, no more groups avail.
                            max_j = -1
                groups.append(this_group)

            if path_to_look:
                with open(create_file_dir("%s/grouped/%s.txt" % (path_to_look, self.name)), "w+") as f:
                    for group in groups:
                        f.write(" ".join([str(elem) for elem in group]) + "\n")

        return groups
