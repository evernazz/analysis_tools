from analysis_tools.base_object import BaseObject


class Process(BaseObject):
    def __init__(self, name, label, *args, **kwargs):
        self.label = label
        self.subprocesses = []
        self.parent_process = kwargs.pop("parent_process", None)
        self.color = kwargs.pop("color", (0, 0, 0))
        isData = kwargs.pop("isData", False)
        self.isData = isData
        self.isMC = not isData
        self.isSignal = kwargs.pop("isSignal", False)
        super(Process, self).__init__(name, *args, **kwargs)

    def __repr__(self):
        return "Process(name=%s, label=%s)" % (self.name, self.label)
    
    def add_subprocess(self, process):
        self.subprocesses.append(process)
    
    def get_color(self):
        return self.color
